import React, { FC } from 'react';
import { useHistory } from 'react-router-dom';

import Button, { StyleButton } from '../../Components/UI/Buttons/Button';
interface IPromo {}

const Promo: FC<IPromo> = () => {
    const history = useHistory();

    const handleOnClick = (e: React.MouseEvent): void => {
        e.preventDefault();
        history.replace('/character');
    };
    return (
        <div className={'promo'}>
            <h1 className="title title__promo">
                Найди любимого
                <br />
                персонажа
                <br />
                “Гарри Поттера”
            </h1>
            <h2 className="subtitle subtitle__promo">
                Вы сможете узнать тип героев, их
                <br />
                способности, сильные стороны и недостатки.
            </h2>
            <Button
                stylebutton={StyleButton.COLOR_B09A81}
                id={'btn-start'}
                onClick={(e: React.MouseEvent): void => {
                    handleOnClick(e);
                }}
            >
                Начать
            </Button>
            <div className={'promo__light'}></div>
        </div>
    );
};

export default Promo;
