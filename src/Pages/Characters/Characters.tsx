import qs from 'qs';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';

import { useHistory } from 'react-router-dom';

import Card, { TypePreview } from '../../Components/Simpl/Card/Card';
import Modal from '../../Components/Simpl/Modal/Modal';
import { getDataCards, getDataFullCard } from '../../Core/API/Characters/character.services';
import { getDataGender, getDataRace, getDataSide } from '../../Core/API/Dictionary/dictionary.services';
import Carousel from '../../Core/Components/Carousel/Carousel';
import FormCreateCard from '../../Core/Components/FormCreateCard/FormCreateCard';
import FiltersWrapper from '../../Core/Components/FormFiltres/FiltersWrapper';
import { URL } from '../../Core/Config/api.config';
import { TCard, TFullCard } from '../../Core/Services/Characters/Characters.model';
import { cardsByFactory, fullCardByFactory } from '../../Core/Services/Characters/characters.factory';
import { parametrsFactory } from '../../Core/Services/Dictionary/dictionary.factory';
import { TObjIdValue } from '../../Core/Services/Dictionary/dictionary.model';
import { homeAction } from '../../Core/redux/store/home/home.slice';
import { useAppDispatch, useAppSelector } from '../../Core/redux/store/store.hooks';
const SHOW_CARD: number = 3;

const Characters = (): React.ReactElement => {
    const history = useHistory();
    const homeState = useAppSelector((state) => state.home);
    const dispatch = useAppDispatch();
    const { cardID } = useParams<{ cardID: string }>();
    const { searchParametres } = useParams<{ searchParametres: string }>();

    const showModal = (): React.ReactElement | undefined => {
        const closeModal = (e: React.MouseEvent): void => {
            e.preventDefault();
            history.replace(`/character/sort/${qs.stringify(homeState.getData)}`);
        };
        if (location.pathname === `/character/new`) {
            return (
                <Modal handleCloseModal={closeModal}>
                    <FormCreateCard></FormCreateCard>
                </Modal>
            );
        }

        if (location.pathname === `/character/ID=${cardID}`) {
            return (
                <Modal handleCloseModal={closeModal}>
                    <Card
                        typePreview={TypePreview.FULLPREVIEW}
                        card={homeState.card}
                        handleCloseModal={closeModal}
                    ></Card>
                </Modal>
            );
        }
    };

    useEffect(() => {
        const getDataParameters = async (): Promise<void> => {
            dispatch(
                homeAction.setParametersGender(parametrsFactory((await getDataGender()) as unknown as TObjIdValue[]))
            );
            dispatch(homeAction.setParametersRace(parametrsFactory((await getDataRace()) as unknown as TObjIdValue[])));
            dispatch(homeAction.setParametersSide(parametrsFactory((await getDataSide()) as unknown as TObjIdValue[])));
            if (location.pathname === `/character/ID=${cardID}`) {
                dispatch(
                    homeAction.setCard(fullCardByFactory((await getDataFullCard(cardID)) as unknown as TFullCard))
                );
            }
        };
        void getDataParameters();
        if (location.pathname === `/character/sort/${searchParametres}`) {
            dispatch(homeAction.setGetData({ ...homeState.getData, ...qs.parse(searchParametres) }));
        }
    }, []);

    useEffect(() => {
        const getCards = async (): Promise<void> => {
            dispatch(
                homeAction.setCards(
                    cardsByFactory(
                        (await getDataCards(`${URL}${qs.stringify(homeState.getData)}`)) as unknown as TCard[]
                    )
                )
            );
        };
        void getCards();
        dispatch(homeAction.setNumberSlide(0));

        if (location.pathname !== `/character/ID=${cardID}` && location.pathname !== `/character/new`) {
            history.replace(`/character/sort/${qs.stringify(homeState.getData)}`);
        }
    }, [homeState.getData]);

    return (
        <div className="characters">
            <div className="container">
                <FiltersWrapper />
                <Carousel cards={homeState.cards} currentSlide={homeState.numberSlide} showCardsOnSlide={SHOW_CARD} />
            </div>
            {showModal()}
        </div>
    );
};

export default Characters;
