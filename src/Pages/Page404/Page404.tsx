import React from 'react';
import { Link, useHistory } from 'react-router-dom';

import Button, { StyleButton } from '../../Components/UI/Buttons/Button';
import background404 from '../../assets/icons/background/404.jpg';

const Page404 = (): React.ReactElement => {
    const history = useHistory();

    const handleOnClick = (e: React.MouseEvent): void => {
        e.preventDefault();
        history.replace('/');
    };
    return (
        <div
            style={{
                backgroundImage: `url(${background404})`,
                height: 'calc(100vh - 75px)',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <div
                className="wrapper"
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    height: '336px',
                    color: 'white',
                    textShadow: '1px 1px 2px black',
                }}
            >
                <p
                    style={{
                        fontWeight: 700,
                        fontSize: '180px',
                        lineHeight: 0.7,
                    }}
                >
                    404
                </p>
                <p style={{ fontSize: '18px' }}>Ошибка 404. Такая страница не существует либо она была удалена</p>
                <Link to="/">
                    <Button
                        stylebutton={StyleButton.COLOR_B09A81}
                        onClick={(e: React.MouseEvent): void => {
                            handleOnClick(e);
                        }}
                    >
                        На главную
                    </Button>
                </Link>
            </div>
        </div>
    );
};

export default Page404;
