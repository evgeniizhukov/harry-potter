import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from '../Core/Components/Header/Header';
import Characters from '../Pages/Characters/Characters';
import Page404 from '../Pages/Page404/Page404';
import Promo from '../Pages/Promo/Promo';

const Routes = (): React.ReactElement => {
    return (
        <Router>
            <Header></Header>
            <Switch>
                <Route exact path="/">
                    <Promo />
                </Route>
                <Route exact path="/Home">
                    <Promo />
                </Route>
                <Route exact path="/character/new">
                    <Characters />
                </Route>
                <Route exact path="/character/sort/:searchParametres">
                    <Characters />
                </Route>
                <Route exact path="/character/ID=:cardID">
                    <Characters />
                </Route>

                <Route exact path="/character">
                    <Characters />
                </Route>
                <Route exact path="*">
                    <Page404 />
                </Route>
            </Switch>
        </Router>
    );
};

export default Routes;
