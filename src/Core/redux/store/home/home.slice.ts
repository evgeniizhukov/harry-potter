import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TCard, TFullCard } from '../../../Services/Characters/Characters.model';
import { GetParameters, TObjIdValue } from '../../../Services/Dictionary/dictionary.model';

import { IHomeState } from './home.type';

const initialState: IHomeState = {
    numberSlide: 0,
    previewNumberSlide: 0,
    cards: [],
    card: {
        id: '',
        name: '',
        description: '',
        imageURL: '',
        nameColor: '',
        backgroundColor: '',
        parametersColor: '',
        tag1: '',
        tag2: '',
        tag3: '',
        gender: {
            id: '',
            value: '',
        },
        race: {
            id: '',
            value: '',
        },
        side: {
            id: '',
            value: '',
        },
    },
    parametersGender: [],
    parametersRace: [],
    parametersSide: [],
    getData: {
        values: '',
        gender: [],
        race: [],
        side: [],
    },
};

export const homeSlice = createSlice({
    name: 'home',
    initialState,
    reducers: {
        setGetData: (state, action: PayloadAction<GetParameters>) => {
            state.getData = action.payload;
        },
        setNumberSlide: (state, action: PayloadAction<number>) => {
            state.numberSlide = action.payload;
        },
        setPreviewNumberSlide: (state, action: PayloadAction<number>) => {
            state.previewNumberSlide = action.payload;
        },
        setCards: (state, action: PayloadAction<TCard[]>) => {
            state.cards = action.payload;
        },
        setCard: (state, action: PayloadAction<TFullCard>) => {
            state.card = action.payload;
        },
        setParametersGender: (state, action: PayloadAction<TObjIdValue[]>) => {
            state.parametersGender = action.payload;
        },
        setParametersRace: (state, action: PayloadAction<TObjIdValue[]>) => {
            state.parametersRace = action.payload;
        },
        setParametersSide: (state, action: PayloadAction<TObjIdValue[]>) => {
            state.parametersSide = action.payload;
        },
    },
});

export const homeAction = homeSlice.actions;
