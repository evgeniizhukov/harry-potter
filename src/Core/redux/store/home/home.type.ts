import { TCard, TFullCard } from '../../../Services/Characters/Characters.model';
import { GetParameters, TObjIdValue } from '../../../Services/Dictionary/dictionary.model';

export interface IHomeState {
    numberSlide: number;
    previewNumberSlide: number;
    getData: GetParameters;
    cards: TCard[];
    card: TFullCard;
    parametersGender: TObjIdValue[];
    parametersRace: TObjIdValue[];
    parametersSide: TObjIdValue[];
}
