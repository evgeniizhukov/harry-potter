import { store } from './index';

export type TRootStore = typeof store;
export type TAppState = ReturnType<TRootStore['getState']>;
export type TAppDispatch = TRootStore['dispatch'];
