import { configureStore } from '@reduxjs/toolkit';

import { homeSlice } from './home/home.slice';

export const store = configureStore({
    reducer: {
        home: homeSlice.reducer,
    },
});
