import { TObjIdValue } from '../Dictionary/dictionary.model';

type TAbstractionCard = {
    id: string;
    imageURL: string;
    name: string;
    nameColor: string;
    parametersColor: string;
    backgroundColor: string;
};

export type TFullCard = TAbstractionCard & {
    description: string;
    tag1: string;
    tag2: string;
    tag3: string;
    gender: TObjIdValue;
    race: TObjIdValue;
    side: TObjIdValue;
};

export type TCard = TAbstractionCard & {
    gender: string;
    race: string;
    side: string;
};
export type TDataObjWithCards = {
    content: TCard[];
};
