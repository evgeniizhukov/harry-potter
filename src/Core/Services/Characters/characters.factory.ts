import { TCard, TFullCard } from './Characters.model';

export const cardsByFactory = (model: TCard[]): TCard[] => {
    return model.map((model) => ({
        id: model.id,
        imageURL: model.imageURL,
        name: model.name,
        nameColor: model.nameColor,
        parametersColor: model.parametersColor,
        backgroundColor: model.backgroundColor,
        gender: model.gender,
        race: model.race,
        side: model.side,
    }));
};

export const fullCardByFactory = (model: TFullCard): TFullCard => {
    return {
        id: model.id,
        imageURL: model.imageURL,
        name: model.name,
        nameColor: model.nameColor,
        parametersColor: model.parametersColor,
        backgroundColor: model.backgroundColor,
        tag1: model.tag1,
        tag2: model.tag2,
        tag3: model.tag3,
        description: model.description,
        gender: {
            id: model.gender.id,
            value: model.gender.value,
        },
        race: {
            id: model.race.id,
            value: model.race.value,
        },
        side: {
            id: model.side.id,
            value: model.side.value,
        },
    };
};
