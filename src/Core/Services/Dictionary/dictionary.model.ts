export type TObjIdValue = {
    id: string;
    value: string;
};

export type GetParameters = {
    values: string;
    gender: string[];
    race: string[];
    side: string[];
};
