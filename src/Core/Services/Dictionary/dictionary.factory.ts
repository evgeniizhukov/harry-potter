import { TObjIdValue } from './dictionary.model';

export const parametrsFactory = (model: TObjIdValue[]): TObjIdValue[] => {
    return model.map((model) => ({
        id: model.id,
        value: model.value,
    }));
};
