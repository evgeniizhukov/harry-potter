export const URL: string = 'http://localhost:5000/api/HARRY_POTTER/character?page=0&size=15&';
export const URL_RACE: string = 'http://localhost:5000/api/HARRY_POTTER/race';
export const URL_SIDE: string = 'http://localhost:5000/api/HARRY_POTTER/side';
export const URL_GENDER: string = 'http://localhost:5000/api/HARRY_POTTER/gender';
