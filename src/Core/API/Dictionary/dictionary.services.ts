import { URL_GENDER, URL_RACE, URL_SIDE } from '../../Config/api.config';
import { TObjIdValue } from '../../Services/Dictionary/dictionary.model';

export const getDataGender = async (): Promise<TObjIdValue[]> => {
    try {
        const respByGender: Response = await fetch(URL_GENDER);
        const gender = (await respByGender.json()) as TObjIdValue[];
        return gender;
    } catch (error) {
        throw new Error(`ERROR BY FETCH RESPONSE`);
    }
};

export const getDataRace = async (): Promise<TObjIdValue[]> => {
    try {
        const respByRace: Response = await fetch(URL_RACE);
        const race = (await respByRace.json()) as TObjIdValue[];
        return race;
    } catch (error) {
        throw new Error(`ERROR BY FETCH RESPONSE`);
    }
};

export const getDataSide = async (): Promise<TObjIdValue[]> => {
    try {
        const respBySide: Response = await fetch(URL_SIDE);
        const side = (await respBySide.json()) as TObjIdValue[];
        return side;
    } catch (error) {
        throw new Error(`ERROR BY FETCH RESPONSE`);
    }
};
