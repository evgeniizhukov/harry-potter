import { TCard, TDataObjWithCards, TFullCard } from '../../Services/Characters/Characters.model';

export const getDataCards = async (url: string): Promise<TCard[]> => {
    try {
        const respByCards: Response = await fetch(url);
        const objWithCards = (await respByCards.json()) as TDataObjWithCards;
        return objWithCards.content;
    } catch (error) {
        throw new Error(`ERROR BY FETCH RESPONSE`);
    }
};

export const getDataFullCard = async (cardID: string): Promise<TFullCard> => {
    try {
        const respByFullCard: Response = await fetch(`http://localhost:5000/api/HARRY_POTTER/character/${cardID}`);
        const fullCard = (await respByFullCard.json()) as TFullCard;
        return fullCard;
    } catch (error) {
        throw new Error(`ERROR BY FETCH RESPONSE`);
    }
};

export const postData = (url: string, value: Omit<TFullCard, 'id'>): void => {
    void fetch(url, {
        method: 'POST',
        headers: {
            'content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify(value),
    });
};
