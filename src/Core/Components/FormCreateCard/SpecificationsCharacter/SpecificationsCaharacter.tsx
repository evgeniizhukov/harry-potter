import { ErrorMessage, Field } from 'formik';
import React, { FC, HTMLAttributes, useEffect, useState } from 'react';

import { ColorInput } from '../../../../Components/Simpl/Formik/ColorInput/ColorInput';

import { TextInput } from '../../../../Components/Simpl/Formik/TextInput/TextInput';
import { CHECKBOX_HEIGHT } from '../../../Constants/checkbox.constants';
import { useAppSelector } from '../../../redux/store/store.hooks';

import InputDropDownListRadio from './InputDropDownListRadio/InputDropDownListRadio';

interface ISpecificationsCaharacter extends HTMLAttributes<HTMLDivElement> {
    descriptionLength: number;
    tagsID: string;
    imageURL: string;
    setFieldValue: (nameField: string, value: string) => void;
}

const SpecificationsCaharacter: FC<ISpecificationsCaharacter> = ({
    descriptionLength,
    tagsID,
    imageURL,
    setFieldValue,
    ...props
}) => {
    const DEFAULT_CLASS_FOR_LABEL: string = 'newCard__newName-label';
    const NUMBER_OF_TAGS: number = 4;

    const homeState = useAppSelector((state) => state.home);

    const [showDropDownListGender, setShowDropDownListGender] = useState<boolean>(false);
    const [showDropDownListRace, setShowDropDownListRace] = useState<boolean>(false);
    const [showDropDownListSide, setShowDropDownListSide] = useState<boolean>(false);

    const [valueDropDownListGender, setValueDropDownListGender] = useState<string>('Пол');
    const [valueDropDownListRace, setValueDropDownListRace] = useState<string>('Раса');
    const [valueDropDownListSide, setValueDropDownListSide] = useState<string>('Сторона');

    const handleToggleShowDropDownGender = (): void => setShowDropDownListGender(!showDropDownListGender);
    const handleToggleShowDropDownRace = (): void => setShowDropDownListRace(!showDropDownListRace);
    const handleToggleShowDropDownSide = (): void => setShowDropDownListSide(!showDropDownListSide);

    const handleOutsideClick = (e: MouseEvent): void => {
        const target = e.target as HTMLDivElement;

        if (target.id !== 'genderCreate' && !target.closest('.select')) {
            setShowDropDownListGender(false);
        }
        if (target.id !== 'raceCreate' && !target.closest('.select')) {
            setShowDropDownListRace(false);
        }
        if (target.id !== 'sideCreate' && !target.closest('.select')) {
            setShowDropDownListSide(false);
        }
    };
    useEffect(() => {
        document.addEventListener('click', (e) => handleOutsideClick(e), true);
        return (): void => {
            document.removeEventListener('click', (e) => handleOutsideClick(e), true);
        };
    }, []);

    const handleSetNewValueGender = (value: string): void => {
        setValueDropDownListGender(value);
        setFieldValue('gender.value', value);
    };
    const handleSetNewValueRace = (value: string): void => {
        setValueDropDownListRace(value);
        setFieldValue('race.value', value);
    };
    const handleSetNewValueSide = (value: string): void => {
        setValueDropDownListSide(value);
        setFieldValue('side.value', value);
    };

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setFieldValue('id', e.target.value.replace('  ', ' '));
        const tags: string = e.target.value;
        if (tags.split(' ').length < NUMBER_OF_TAGS) {
            setFieldValue('tag1', tags.split(' ')[0]);
            setFieldValue('tag2', tags.split(' ')[1]);
            setFieldValue('tag3', tags.split(' ')[2]);
        }
    };
    return (
        <div {...props}>
            <TextInput classnamelabel={DEFAULT_CLASS_FOR_LABEL} id={'name'} name={'name'}>
                Добавить имя
            </TextInput>
            <div className={'newCard__wrapper-for-filtrs'}>
                <div className="align-items__center">
                    <InputDropDownListRadio
                        id={'genderCreate'}
                        value={valueDropDownListGender}
                        handleToggleShowDropDown={handleToggleShowDropDownGender}
                        parametrCount={homeState.getData.gender}
                        showDropDownList={showDropDownListGender}
                        amountElements={homeState.parametersGender}
                        heightElement={CHECKBOX_HEIGHT}
                        setValue={handleSetNewValueGender}
                        name={'gender.value'}
                        style={{
                            width: '100%',
                            height: '30px',
                            padding: '11px 30px 11px 11px',
                            fontSize: '14px',
                        }}
                    />
                    <ErrorMessage name="gender.value" className="error" component="p" />
                </div>
                <div className="align-items__center">
                    <InputDropDownListRadio
                        id={'raceCreate'}
                        value={valueDropDownListRace}
                        handleToggleShowDropDown={handleToggleShowDropDownRace}
                        parametrCount={homeState.getData.race}
                        showDropDownList={showDropDownListRace}
                        amountElements={homeState.parametersRace}
                        heightElement={CHECKBOX_HEIGHT}
                        setValue={handleSetNewValueRace}
                        name={'race.value'}
                        style={{
                            width: '100%',
                            height: '30px',
                            padding: '11px 30px 11px 11px',
                            fontSize: '14px',
                        }}
                    />
                    <ErrorMessage name="race.value" className="error" component="p" />
                </div>
                <div className="align-items__center">
                    <InputDropDownListRadio
                        id={'sideCreate'}
                        value={valueDropDownListSide}
                        handleToggleShowDropDown={handleToggleShowDropDownSide}
                        parametrCount={homeState.getData.side}
                        showDropDownList={showDropDownListSide}
                        amountElements={homeState.parametersSide}
                        heightElement={CHECKBOX_HEIGHT}
                        setValue={handleSetNewValueSide}
                        name={'side.value'}
                        style={{
                            width: '100%',
                            height: '30px',
                            padding: '11px 30px 11px 11px',
                            fontSize: '14px',
                        }}
                    />
                    <ErrorMessage name="side.value" className="error" component="p" />
                </div>
            </div>

            <label htmlFor={'description'} className={`${DEFAULT_CLASS_FOR_LABEL} label__textarea`}>
                Добавить описание
                <Field
                    id={'description'}
                    className={'newCard__newName btn textarea'}
                    name={'description'}
                    as="textarea"
                    maxLength={100}
                    style={{ fontSize: '14px', padding: '10px' }}
                ></Field>
                <div className="currentSimvols" style={{ opacity: '.5' }}>
                    <div className="currentSimvols__current">{descriptionLength}</div>
                    <div className="currentSimvols__maxCurrent">/100</div>
                </div>
            </label>
            <label htmlFor={'id'} className={DEFAULT_CLASS_FOR_LABEL}>
                Добавить теги
                <Field
                    className="newCard__newName btn"
                    style={{ fontSize: '14px', paddingLeft: '10px' }}
                    name={'id'}
                    id={'id'}
                    value={tagsID}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                        handleOnChange(e);
                    }}
                ></Field>
                <ErrorMessage name="id" className="error" component="p"></ErrorMessage>
                <div className="currentSimvols" style={{ opacity: '.5' }}>
                    <div
                        className="currentSimvols__current"
                        style={tagsID.split(' ').length >= NUMBER_OF_TAGS ? { color: 'red' } : { color: '' }}
                    >
                        {!tagsID.length ? 0 : tagsID.split(' ').length}
                    </div>
                    <div className="currentSimvols__maxCurrent">/3</div>
                </div>
            </label>

            <div className="descriptionBottom">
                <label htmlFor={'imageURL'} className={'descriptionBottom__img-wrapper'}>
                    <p>Добавить фото</p>
                    <div
                        className={'newCard__newPhotoWrapper'}
                        style={
                            imageURL
                                ? {
                                      backgroundImage: `url(${imageURL})`,
                                      backgroundSize: 'cover',
                                      backgroundPosition: 'center center',
                                  }
                                : {}
                        }
                    ></div>
                    <Field
                        id={'imageURL'}
                        className={'newCard__setNewPhoto'}
                        placeholder="URL изображения"
                        name={'imageURL'}
                    ></Field>
                </label>
                <div className="descriptionBottom__colors">
                    <p className={'descriptionBottom__colors-title'}>Выбрать цвет</p>

                    <ColorInput id={'nameColor'} name={'nameColor'}>
                        Цвет имени
                    </ColorInput>
                    <ColorInput id={'backgroundColor'} name={'backgroundColor'}>
                        Цвет фона параметров
                    </ColorInput>
                    <ColorInput id={'parametersColor'} name={'parametersColor'}>
                        Цвет параметров
                    </ColorInput>
                </div>
            </div>
        </div>
    );
};

export default SpecificationsCaharacter;
