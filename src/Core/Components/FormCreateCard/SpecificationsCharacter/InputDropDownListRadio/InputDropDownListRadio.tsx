import React, { FC, HTMLAttributes } from 'react';

import CustomRadio from '../../../../../Components/Simpl/CustomRadio/CustomRadio';
import DropDownListWrapper from '../../../../../Components/Simpl/DropDownListWrapper/DropDownListWrapper';

import InputDropDownListWrapper from '../../../../../Components/Simpl/InputDropDownListWrapper/InputDropDownListWrapper';
import { TObjIdValue } from '../../../../Services/Dictionary/dictionary.model';

interface IInputDropDownListRadio extends HTMLAttributes<HTMLLabelElement> {
    id: string;
    value: string;
    name: string;
    handleToggleShowDropDown: () => void;
    parametrCount?: string[];
    amountElements: TObjIdValue[];
    heightElement: number;
    showDropDownList: boolean;
    setValue: (value: string) => void;
}

const InputDropDownListRadio: FC<IInputDropDownListRadio> = ({
    id,
    value,
    handleToggleShowDropDown,
    parametrCount,
    showDropDownList,
    amountElements,
    heightElement,
    setValue,
    ...props
}) => {
    return (
        <InputDropDownListWrapper
            id={id}
            value={value}
            handleToggleShowDropDown={handleToggleShowDropDown}
            parametrCount={parametrCount}
            style={{ height: '30px', fontSize: '14px' }}
            {...props}
        >
            <DropDownListWrapper
                showDropDownList={showDropDownList}
                amountElements={amountElements.length}
                heightElement={heightElement}
            >
                {amountElements.map((item, i) => (
                    <CustomRadio id={item.id + 'q'} key={i} name={id} value={item.value} setValue={setValue}>
                        {item.value}
                    </CustomRadio>
                ))}
            </DropDownListWrapper>
        </InputDropDownListWrapper>
    );
};

export default InputDropDownListRadio;
