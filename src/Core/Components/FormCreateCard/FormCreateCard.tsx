import { Formik, Form } from 'formik';
import qs from 'qs';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import * as yup from 'yup';

import Button, { StyleButton } from '../../../Components/UI/Buttons/Button';
import { postData } from '../../API/Characters/character.services';

import { ERROR_REQUIRE_FIELD, ERROR_UP_TO_TREE_WORLDS } from '../../Constants/message.constants';
import { TFullCard } from '../../Services/Characters/Characters.model';
import { homeAction } from '../../redux/store/home/home.slice';
import { useAppDispatch, useAppSelector } from '../../redux/store/store.hooks';

import PreviewCard from './PreviewCard/PreviewCard';
import SpecificationsCaharacter from './SpecificationsCharacter/SpecificationsCaharacter';

interface IFormCreateCard {}

interface MyFormValues extends TFullCard {}

const FormCreateCard: FC<IFormCreateCard> = (): React.ReactElement => {
    const history = useHistory();
    const homeState = useAppSelector((state) => state.home);
    const dispatch = useAppDispatch();

    const setPreviewNumberSlide = (numberSlide: number): void => {
        dispatch(homeAction.setPreviewNumberSlide(numberSlide));
    };
    const handleOnChangeSubmitForm = (value: Omit<MyFormValues, 'id'>): void => {
        homeState.parametersRace.forEach((e) => {
            if (e.value === value.race.value) {
                value.race.id = e.id;
            }
        });
        homeState.parametersGender.forEach((e) => {
            if (e.value === value.gender.value) {
                value.gender.id = e.id;
            }
        });
        homeState.parametersSide.forEach((e) => {
            if (e.value === value.side.value) {
                value.side.id = e.id;
            }
        });
        const newArray: TFullCard | Omit<TFullCard, 'id'> = JSON.parse(JSON.stringify(value)) as TFullCard;
        delete newArray['id'];
        postData('http://localhost:5000/api/HARRY_POTTER/character', newArray);

        handleCloseModal();
    };

    const handleCloseModal = (e?: React.MouseEvent): void => {
        e?.preventDefault();
        history.replace(`/character/sort/${qs.stringify(homeState.getData)}`);
    };

    const validationSchema = yup.object().shape({
        name: yup.string().required(ERROR_REQUIRE_FIELD),
        gender: yup.object().shape({
            value: yup.string().required(ERROR_REQUIRE_FIELD),
        }),
        side: yup.object().shape({
            value: yup.string().required(ERROR_REQUIRE_FIELD),
        }),

        race: yup.object().shape({
            value: yup.string().required(ERROR_REQUIRE_FIELD),
        }),
        id: yup //Строка с тегами
            .string()
            .matches(/^([А-я]+){0,1}(\s[А-я]+){0,2}$/, ERROR_UP_TO_TREE_WORLDS),
    });
    const initialValues: MyFormValues = {
        id: '', //Строка с тегами
        name: '',
        description: '',
        imageURL: '',
        nameColor: '#ffffff',
        backgroundColor: '#ffffff',
        parametersColor: '#000000',
        tag1: '',
        tag2: '',
        tag3: '',
        gender: {
            id: '',
            value: '',
        },
        race: {
            id: '',
            value: '',
        },
        side: {
            id: '',
            value: '',
        },
    };
    return (
        <div className={'newCard'}>
            <Formik
                initialValues={initialValues}
                validateOnBlur
                onSubmit={(values): void => {
                    handleOnChangeSubmitForm(values);
                }}
                validationSchema={validationSchema}
            >
                {({ values, isValid, dirty, setFieldValue }): React.ReactElement => (
                    <Form className="form newCard__description">
                        <SpecificationsCaharacter
                            descriptionLength={values.description.length}
                            tagsID={values.id}
                            imageURL={values.imageURL}
                            setFieldValue={setFieldValue}
                        />
                        <Button disabled={!isValid && !dirty} type={'submit'} stylebutton={StyleButton.COLOR_GREEN}>
                            Сохранить
                        </Button>
                        <div className="newCard__verticalLine"></div>
                        <PreviewCard
                            card={values}
                            currentSlide={homeState.previewNumberSlide}
                            setPreviewNumberSlide={setPreviewNumberSlide}
                        />
                        <Button
                            onClick={(e: React.MouseEvent): void => {
                                handleCloseModal(e);
                            }}
                            stylebutton={StyleButton.CLOSE}
                        />
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default FormCreateCard;
