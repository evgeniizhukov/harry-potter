import { FC, HTMLAttributes, useState } from 'react';

import CarouselNavigation from '../../../../Components/Ordinary/CarouselNavigation/CarouselNavigation';
import Card, { TypePreview } from '../../../../Components/Simpl/Card/Card';
import { TFullCard } from '../../../Services/Characters/Characters.model';

interface IpreviewCard extends HTMLAttributes<HTMLDivElement> {
    card: TFullCard;
    currentSlide: number;
    setPreviewNumberSlide: (numberSlide: number) => void;
}

const PreviewCard: FC<IpreviewCard> = ({ card, currentSlide, setPreviewNumberSlide, ...props }) => {
    const [viewFirstWrapper, setviewFirstWrapper] = useState<boolean>(true);

    const setCurrentSlide = (numberSlide: number): void => {
        setPreviewNumberSlide(numberSlide);
        setviewFirstWrapper(!viewFirstWrapper);
    };

    return (
        <div className="newCard__preview" {...props}>
            <p className={'newCard__preview_title'}>Предварительный просмотр</p>
            <div className="newCard__preview_mainContent" style={{ position: 'relative' }}>
                <Card typePreview={viewFirstWrapper ? TypePreview.MINIPREVIEW : TypePreview.MINI} card={card}></Card>
                <CarouselNavigation
                    lengthSlider={2}
                    currentSlide={currentSlide}
                    showCardOnOneSlide={1}
                    setNumberSlide={setCurrentSlide}
                    numberDotMaximum={2}
                    style={{ bottom: '-60px', width: '150px' }}
                ></CarouselNavigation>
                <div
                    style={{
                        display: 'flex',
                        width: '100px',
                        height: '15px',
                        position: 'absolute',
                        top: '-25px',
                        left: 0,
                        color: '#B09A81',
                        justifyContent: 'space-between',
                        cursor: 'pointer',
                    }}
                    onClick={(): void => {
                        setCurrentSlide(currentSlide ? 0 : 1);
                    }}
                >
                    <div className="title1" style={viewFirstWrapper ? {} : { color: 'rgba(176, 154, 129, .5)' }}>
                        Вид 1
                    </div>
                    <div className="title2" style={!viewFirstWrapper ? {} : { color: 'rgba(176, 154, 129, .5)' }}>
                        Вид 2
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PreviewCard;
