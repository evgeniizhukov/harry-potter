import React, { FC } from 'react';
import { useHistory } from 'react-router';

import Button, { StyleButton } from '../../../Components/UI/Buttons/Button';
import Input, { StyleInput } from '../../../Components/UI/Input/Input';

import { homeAction } from '../../redux/store/home/home.slice';
import { useAppDispatch, useAppSelector } from '../../redux/store/store.hooks';

import Filtres from './Filters/Filtres';

interface IFiltersWrapper {}

const FiltersWrapper: FC<IFiltersWrapper> = () => {
    const homeState = useAppSelector((state) => state.home);
    const dispatch = useAppDispatch();
    const history = useHistory();

    const handleOnClick = (e: React.MouseEvent): void => {
        e.preventDefault();
        history.replace('/character/new');
    };

    const handleOnBlure = (e: string): void => {
        dispatch(
            homeAction.setGetData({
                ...homeState.getData,
                values: e,
            })
        );
    };
    return (
        <form className={'input'}>
            <Input
                onBlur={(e: React.ChangeEvent): void => {
                    handleOnBlure((e.target as HTMLInputElement).value);
                }}
                id="search"
                styleinput={StyleInput.TEXT}
                name="search"
                placeholder="Поиск"
                style={{ marginBottom: '15px', width: '100%', height: '50px', padding: '0 20px' }}
            ></Input>
            <Filtres />
            <Button
                stylebutton={StyleButton.ADD}
                onClick={(e: React.MouseEvent): void => {
                    handleOnClick(e);
                }}
            >
                Добавить
            </Button>
        </form>
    );
};

export default FiltersWrapper;
