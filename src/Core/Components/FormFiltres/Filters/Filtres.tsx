import React, { FC, useEffect, useState } from 'react';

import { CHECKBOX_HEIGHT } from '../../../Constants/checkbox.constants';

import { homeAction } from '../../../redux/store/home/home.slice';
import { useAppDispatch, useAppSelector } from '../../../redux/store/store.hooks';

import InputDropDownList from './InputDropDownList/InputDropDownList';

interface IFiltres {}

const Filtres: FC<IFiltres> = () => {
    const homeState = useAppSelector((state) => state.home);
    const dispatch = useAppDispatch();

    const [showDropDownListGender, setShowDropDownListGender] = useState<boolean>(false);
    const [showDropDownListRace, setShowDropDownListRace] = useState<boolean>(false);
    const [showDropDownListSide, setShowDropDownListSide] = useState<boolean>(false);

    const handleOutsideClick = (e: MouseEvent): void => {
        const target = e.target as HTMLDivElement;

        if (target.id !== 'gender' && !target.closest('.select')) {
            console.log(target.closest('select'));
            setShowDropDownListGender(false);
        }
        if (target.id !== 'race' && !target.closest('.select')) {
            setShowDropDownListRace(false);
        }
        if (target.id !== 'side' && !target.closest('.select')) {
            setShowDropDownListSide(false);
        }
    };
    useEffect(() => {
        document.addEventListener('click', (e) => handleOutsideClick(e));
        return (): void => {
            document.removeEventListener('click', (e) => handleOutsideClick(e));
        };
    }, []);

    const handleToggleShowDropDownGender = (): void => {
        setShowDropDownListGender(!showDropDownListGender);
    };
    const handleToggleShowDropDownRace = (): void => {
        setShowDropDownListRace(!showDropDownListRace);
    };
    const handleToggleShowDropDownSide = (): void => {
        setShowDropDownListSide(!showDropDownListSide);
    };

    // set state in checkbox by loaded page
    const setInitialStateChecbox = (id: string, getParametr: string): boolean => {
        for (const key in homeState.getData) {
            if (key === getParametr) {
                const keys: string[] = homeState.getData[key] as string[];
                if (keys.includes(id)) {
                    return true;
                }
            }
        }
        return false;
    };

    // toggle getParameters in STATE
    const setUrl = (getParameters: string, id: string): void => {
        for (const key in homeState.getData) {
            if (key === getParameters) {
                const keys: string[] = homeState.getData[key] as string[];
                if (keys.includes(id)) {
                    dispatch(
                        homeAction.setGetData({
                            ...homeState.getData,
                            [getParameters]: keys.filter((el) => el !== id),
                        })
                    );
                } else {
                    dispatch(
                        homeAction.setGetData({
                            ...homeState.getData,
                            [getParameters]: keys.slice(0).concat(id),
                        })
                    );
                }
            }
        }
    };

    return (
        <div className={'input__filters'}>
            <InputDropDownList
                id={'gender'}
                value={homeState.getData.gender?.length ? `${'Пол'}: ${homeState.getData.gender.length}` : `${'Пол'}`}
                handleToggleShowDropDown={handleToggleShowDropDownGender}
                parametrCount={homeState.getData.gender}
                showDropDownList={showDropDownListGender}
                amountElements={homeState.parametersGender}
                heightElement={CHECKBOX_HEIGHT}
                setUrl={setUrl}
                setInitialStateChecbox={setInitialStateChecbox}
                style={{
                    width: '100%',
                    height: '36px',
                    padding: '11px 30px 11px 11px',
                    fontSize: '16px',
                }}
            />
            <InputDropDownList
                id={'race'}
                value={homeState.getData.race?.length ? `${'Раса'}: ${homeState.getData.race.length}` : `${'Раса'}`}
                handleToggleShowDropDown={handleToggleShowDropDownRace}
                parametrCount={homeState.getData.race}
                showDropDownList={showDropDownListRace}
                amountElements={homeState.parametersRace}
                heightElement={CHECKBOX_HEIGHT}
                setUrl={setUrl}
                setInitialStateChecbox={setInitialStateChecbox}
                style={{
                    width: '100%',
                    height: '36px',
                    padding: '11px 30px 11px 11px',
                    fontSize: '16px',
                }}
            />
            <InputDropDownList
                id={'side'}
                value={
                    homeState.getData.side?.length ? `${'Сторона'}: ${homeState.getData.side.length}` : `${'Сторона'}`
                }
                handleToggleShowDropDown={handleToggleShowDropDownSide}
                parametrCount={homeState.getData.side}
                showDropDownList={showDropDownListSide}
                amountElements={homeState.parametersSide}
                heightElement={CHECKBOX_HEIGHT}
                setUrl={setUrl}
                setInitialStateChecbox={setInitialStateChecbox}
                style={{
                    width: '100%',
                    height: '36px',
                    padding: '11px 30px 11px 11px',
                    fontSize: '16px',
                }}
            />
        </div>
    );
};

export default Filtres;
