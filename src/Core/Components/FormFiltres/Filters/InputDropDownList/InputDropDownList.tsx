import React, { AllHTMLAttributes, FC } from 'react';

import CustomChecbox from '../../../../../Components/Simpl/CustomChecbox/CustomChecbox';
import DropDownListWrapper from '../../../../../Components/Simpl/DropDownListWrapper/DropDownListWrapper';

import InputDropDownListWrapper from '../../../../../Components/Simpl/InputDropDownListWrapper/InputDropDownListWrapper';
import { TObjIdValue } from '../../../../Services/Dictionary/dictionary.model';

interface IInputDropDownList extends AllHTMLAttributes<HTMLLabelElement> {
    id: string;
    value: string;
    handleToggleShowDropDown: () => void;
    parametrCount?: string[];
    amountElements: TObjIdValue[];
    heightElement: number;
    showDropDownList: boolean;
    setUrl?: (getParameters: string, id: string) => void;
    setInitialStateChecbox?: (id: string, getParametr: string) => boolean;
}

const InputDropDownList: FC<IInputDropDownList> = ({
    id,
    value,
    handleToggleShowDropDown,
    parametrCount,
    showDropDownList,
    amountElements,
    heightElement,
    setUrl,
    setInitialStateChecbox,
    ...props
}) => {
    return (
        <InputDropDownListWrapper
            id={id}
            value={value}
            handleToggleShowDropDown={handleToggleShowDropDown}
            parametrCount={parametrCount}
            {...props}
        >
            <DropDownListWrapper
                showDropDownList={showDropDownList}
                amountElements={amountElements.length}
                heightElement={heightElement}
            >
                {amountElements.map((item, i) => (
                    <CustomChecbox
                        id={item.id}
                        key={i}
                        getParametr={id}
                        setUrl={setUrl}
                        setInitialStateChecbox={setInitialStateChecbox}
                    >
                        {item.value}
                    </CustomChecbox>
                ))}
            </DropDownListWrapper>
        </InputDropDownListWrapper>
    );
};

export default InputDropDownList;
