import { FC, HTMLAttributes } from 'react';
import { useHistory } from 'react-router-dom';

import CarouselNavigation from '../../../Components/Ordinary/CarouselNavigation/CarouselNavigation';
import Card, { TypePreview } from '../../../Components/Simpl/Card/Card';

import { getDataFullCard } from '../../API/Characters/character.services';
import { TCard, TFullCard } from '../../Services/Characters/Characters.model';
import { fullCardByFactory } from '../../Services/Characters/characters.factory';
import { homeAction } from '../../redux/store/home/home.slice';
import { useAppDispatch } from '../../redux/store/store.hooks';

interface ICarousel extends HTMLAttributes<HTMLDivElement> {
    cards: TCard[];
    currentSlide: number;
    showCardsOnSlide: number;
}

const Carousel: FC<ICarousel> = ({ cards, currentSlide, showCardsOnSlide, ...props }) => {
    const SHOW_CARD: number = showCardsOnSlide;
    const history = useHistory();
    const dispatch = useAppDispatch();

    const handleOnClick = async (cardID: string): Promise<void> => {
        dispatch(homeAction.setCard(fullCardByFactory((await getDataFullCard(cardID)) as unknown as TFullCard)));
        history.replace(`/character/ID=${cardID}`);
    };

    const setNumberSlide = (numberSlide: number): void => {
        dispatch(homeAction.setNumberSlide(numberSlide));
    };
    return (
        <div className={'slider'} {...props}>
            <CarouselNavigation
                lengthSlider={cards.length}
                currentSlide={currentSlide}
                showCardOnOneSlide={SHOW_CARD}
                setNumberSlide={setNumberSlide}
                numberDotMaximum={3}
            ></CarouselNavigation>
            <div className="slider__inner" style={{ left: '0px' }}>
                {[...cards.slice(currentSlide, currentSlide + SHOW_CARD)].map((card) => (
                    <Card
                        key={card.id}
                        card={card}
                        typePreview={TypePreview.MAIN}
                        getCard={(): void => {
                            void handleOnClick(card.id);
                        }}
                    ></Card>
                ))}
            </div>
        </div>
    );
};

export default Carousel;
