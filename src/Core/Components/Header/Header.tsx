import { FC, HTMLAttributes } from 'react';

import Logo from '../../../Components/Simpl/Logo/Logo';

import logo from '../../../assets/icons/logo/logo_Header.png';

import NavBar from './NavBar/NavBar';

interface IMainPage extends HTMLAttributes<HTMLDivElement> {}

const Header: FC<IMainPage> = (props) => {
    return (
        <header className="header" {...props}>
            <Logo url={'/home'} src={logo} alt="Harry Potter"></Logo>

            <NavBar />
        </header>
    );
};

export default Header;
