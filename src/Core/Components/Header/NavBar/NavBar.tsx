import { FC, HTMLAttributes } from 'react';

import LinkItemWrapper from '../../../../Components/Simpl/LinkItemWrapper/LinkItemWrapper';
import LinkListWrapper from '../../../../Components/Simpl/LinkListWrapper/LinkListWrapper';

import NavBarWrapper from '../../../../Components/Simpl/NavBarWrapper/NavBarWrapper';

interface INavBar extends HTMLAttributes<HTMLDivElement> {}

const NavBar: FC<INavBar> = ({ ...props }) => {
    return (
        <NavBarWrapper {...props}>
            <LinkListWrapper>
                <LinkItemWrapper url={'/Home'}>Главная</LinkItemWrapper>
                <LinkItemWrapper url={'/character'}>Персонажи</LinkItemWrapper>
            </LinkListWrapper>
        </NavBarWrapper>
    );
};

export default NavBar;
