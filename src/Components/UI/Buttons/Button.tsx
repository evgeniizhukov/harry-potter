import React, { ButtonHTMLAttributes, FC } from 'react';

export enum StyleButton {
    COLOR_B09A81 = 'btn',
    COLOR_GREEN = 'btn btn_green',
    ADD = 'btn btn_add',
    CLOSE = 'modal__close',
}

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
    stylebutton: StyleButton;
}

const Button: FC<IButton> = ({ ...props }) => {
    return (
        <button {...props} className={props.stylebutton}>
            {props.children}
        </button>
    );
};

export default Button;
