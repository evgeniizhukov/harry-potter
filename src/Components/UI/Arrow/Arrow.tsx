import React, { AllHTMLAttributes, FC } from 'react';

interface IArrow extends AllHTMLAttributes<HTMLDivElement> {}

const Arrow: FC<IArrow> = ({ ...props }) => {
    return <div {...props} className={'slider__arrow'}></div>;
};

export default Arrow;
