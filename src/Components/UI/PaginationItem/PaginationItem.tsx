import React, { AllHTMLAttributes, FC } from 'react';

interface IPaginationItem extends AllHTMLAttributes<HTMLDivElement> {}

const PaginationItem: FC<IPaginationItem> = (props) => {
    return <div className={'slider__nav-circle'} {...props}></div>;
};

export default PaginationItem;
