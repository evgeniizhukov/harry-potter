import React, { FC, InputHTMLAttributes } from 'react';

export enum StyleInput {
    COLOR = 'input__color',
    TEXT = 'newCard__newName btn',
}

interface IInput extends InputHTMLAttributes<HTMLInputElement> {
    styleinput?: StyleInput;
}

const Input: FC<IInput> = ({ ...props }) => {
    return <input {...props} className={props.styleinput}></input>;
};

export default Input;
