import { useField } from 'formik';
import React, { FC } from 'react';

import Input, { StyleInput } from '../../../UI/Input/Input';

interface IMyTextInput {
    name: string;
    classnamelabel: string;
    id: string;
}

export const TextInput: FC<IMyTextInput> = ({ children, ...props }): React.ReactElement => {
    const [field, meta] = useField(props);
    return (
        <>
            <label htmlFor={props.id} className={props.classnamelabel}>
                {children}

                <Input
                    {...props}
                    {...field}
                    styleinput={StyleInput.TEXT}
                    style={{ paddingLeft: '10px', fontSize: '14px' }}
                ></Input>
            </label>
            {meta.touched && meta.error ? <p className="error">{meta.error}</p> : null}
        </>
    );
};
