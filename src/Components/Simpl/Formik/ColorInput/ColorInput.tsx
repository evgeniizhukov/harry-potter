import { useField } from 'formik';
import React, { FC } from 'react';

import Input, { StyleInput } from '../../../UI/Input/Input';

interface IMyTextInput {
    name: string;
    id: string;
}

export const ColorInput: FC<IMyTextInput> = ({ children, ...props }): React.ReactElement => {
    const [field, meta] = useField({ ...props, type: 'color' });
    return (
        <>
            <label htmlFor={props.id} className={'label__color'}>
                <p className={'label__color-text'}>{children}</p>

                <Input
                    {...props}
                    {...field}
                    styleinput={StyleInput.COLOR}
                    type={'color'}
                    style={{ fontSize: '14px' }}
                ></Input>
            </label>
            {meta.touched && meta.error ? <p className="error">{meta.error}</p> : null}
        </>
    );
};
