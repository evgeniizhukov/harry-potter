import React, { FC, HTMLAttributes } from 'react';

interface IModal extends HTMLAttributes<HTMLDivElement> {
    handleCloseModal: (e: React.MouseEvent) => void;
}

const Modal: FC<IModal> = ({ handleCloseModal, ...props }): React.ReactElement => {
    return (
        <div
            className="modal"
            {...props}
            onClick={(e: React.MouseEvent): void => {
                handleCloseModal(e);
            }}
        >
            <div
                onClick={(e: React.MouseEvent): void => {
                    e.stopPropagation();
                }}
            >
                {props.children}
            </div>
        </div>
    );
};

export default Modal;
