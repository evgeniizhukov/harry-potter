import React, { FC, HTMLAttributes } from 'react';

interface ILinkList extends HTMLAttributes<HTMLUListElement> {}

const LinkList: FC<ILinkList> = ({ ...props }) => {
    return (
        <ul className={'header__links'} {...props}>
            {props.children}
        </ul>
    );
};

export default LinkList;
