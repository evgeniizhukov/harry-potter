import { FC } from 'react';

import { TypePreview } from '../Card';

interface IImageCard {
    typePreview: TypePreview;
    imageURL: string;
}

const ImageCard: FC<IImageCard> = ({ typePreview, imageURL, ...props }) => {
    return (
        <div className={`${typePreview}__img-wrapper`} style={{ background: '#B09A81' }} {...props}>
            <div
                className={`${typePreview}__img`}
                style={{
                    backgroundImage: `url(${imageURL})`,
                    backgroundSize: 'cover',
                    backgroundPosition: 'center center',
                }}
            ></div>
        </div>
    );
};

export default ImageCard;
