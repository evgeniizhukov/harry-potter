import { FC } from 'react';

import { TObjIdValue } from '../../../../Core/Services/Dictionary/dictionary.model';

import { TypePreview } from '../Card';
interface IDescriptionCard {
    typePreview: TypePreview;
    nameColor: string;
    name: string;
    backgroundColor: string;
    parametersColor: string;
    gender: string | TObjIdValue;
    race: string | TObjIdValue;
    side: string | TObjIdValue;
    description?: string;
}

const DescriptionCard: FC<IDescriptionCard> = ({
    typePreview,
    nameColor,
    name,
    backgroundColor,
    parametersColor,
    gender,
    race,
    side,
    description,
    ...props
}) => {
    return (
        <div className={`${typePreview}__description`} {...props}>
            <p className={`${typePreview}__title`} style={{ color: nameColor }}>
                {name}
            </p>
            <div className={`${typePreview}__specifications`} style={{ background: `${backgroundColor}` }}>
                <div
                    className={`${typePreview}__wrapper`}
                    style={{
                        color: parametersColor,
                        borderBottom: `1px solid ${parametersColor}`,
                    }}
                >
                    Пол
                    <p className={`${typePreview}__sex`}>{typeof gender === 'object' ? gender.value : gender}</p>
                </div>
                <div
                    className={`${typePreview}__wrapper`}
                    style={{
                        color: parametersColor,
                        borderBottom: `1px solid ${parametersColor}`,
                    }}
                >
                    Раса
                    <p className={`${typePreview}__race`}>{typeof race === 'object' ? race.value : race}</p>
                </div>
                <div
                    className={`${typePreview}__wrapper`}
                    style={{
                        color: parametersColor,
                    }}
                >
                    Сторона
                    <p className={`${typePreview}__side`}>{typeof side === 'object' ? side.value : side}</p>
                </div>
            </div>
            {description ? <p className={`${typePreview}__textContent`}>{description}</p> : null}
        </div>
    );
};

export default DescriptionCard;
