import React, { FC } from 'react';

import { TCard, TFullCard } from '../../../Core/Services/Characters/Characters.model';
import Button, { StyleButton } from '../../UI/Buttons/Button';

import DescriptionCard from './DescriptionCard/DescriptionCard';
import ImageCard from './ImageCard/ImageCard';

export enum TypePreview {
    MAIN = 'card',
    FULLPREVIEW = 'modal',
    MINI = 'previewSecond',
    MINIPREVIEW = 'preview',
}

interface ICard {
    card: TCard | TFullCard;
    handleCloseModal?: (e: React.MouseEvent) => void;
    typePreview: TypePreview;
    getCard?: () => void;
}
const Card: FC<ICard> = ({ card, handleCloseModal, typePreview, getCard, ...props }): JSX.Element => {
    return (
        <div className={`${typePreview}__card`} onClick={getCard} {...props}>
            <div className={`${typePreview}__wrapper`} style={{ height: '100%' }}>
                <DescriptionCard
                    typePreview={typePreview}
                    nameColor={card.nameColor}
                    name={card.name}
                    backgroundColor={card.backgroundColor}
                    parametersColor={card.parametersColor}
                    gender={card.gender}
                    race={card.race}
                    side={card.side}
                    description={'description' in card ? card.description : ''}
                />

                <ImageCard typePreview={typePreview} imageURL={card.imageURL} />
                {'description' in card ? (
                    <div className={`${typePreview}__tags`}>
                        {!!card.tag1 && <div className={`${typePreview}__tag1`}>{card.tag1}</div>}
                        {!!card.tag2 && <div className={`${typePreview}__tag2`}>{card.tag2}</div>}
                        {!!card.tag3 && <div className={`${typePreview}__tag3`}>{card.tag3}</div>}
                    </div>
                ) : null}
                {typePreview === TypePreview.FULLPREVIEW ? (
                    <Button onClick={handleCloseModal} stylebutton={StyleButton.CLOSE} />
                ) : null}
            </div>
        </div>
    );
};

export default Card;
