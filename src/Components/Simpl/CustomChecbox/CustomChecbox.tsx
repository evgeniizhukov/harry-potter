import React, { AllHTMLAttributes, FC, useState } from 'react';

import Input from '../../UI/Input/Input';

interface IChecbox extends AllHTMLAttributes<HTMLLabelElement> {
    id: string;
    getParametr: string;
    setUrl?: (getParameters: string, id: string) => void;
    setInitialStateChecbox?: (id: string, getParametr: string) => boolean;
}

const Checbox: FC<IChecbox> = ({ id, getParametr, setUrl, setInitialStateChecbox, ...props }) => {
    const INITIAL_STATE_CHECKBOX: boolean = setInitialStateChecbox ? setInitialStateChecbox(id, getParametr) : false;
    const [checked, setChecked] = useState(INITIAL_STATE_CHECKBOX);

    const changeCheckbox = (e: HTMLInputElement): void => {
        if (e.matches('input')) {
            setChecked(!checked);
            setUrl ? setUrl(getParametr, id) : null;
        }
    };
    return (
        <label
            onClick={(e: React.MouseEvent): void => {
                changeCheckbox(e.target as HTMLInputElement);
            }}
            className="option"
            htmlFor={id}
            {...props}
        >
            <Input readOnly id={id} defaultChecked={checked} type={'checkbox'} value=""></Input>

            <span className="option__checkbox"></span>
            {props.children}
        </label>
    );
};

export default Checbox;
