import { AllHTMLAttributes, FC } from 'react';

import Arrow from '../../UI/Arrow/Arrow';
import Input, { StyleInput } from '../../UI/Input/Input';
interface IInputDropDownListWrapper extends AllHTMLAttributes<HTMLLabelElement> {
    id: string;
    value: string;
    handleToggleShowDropDown: () => void;
    parametrCount?: string[];
}

const InputDropDownListWrapper: FC<IInputDropDownListWrapper> = ({
    id,
    value,
    handleToggleShowDropDown,
    parametrCount,
    name,
    style,
    ...props
}) => {
    return (
        <label htmlFor={id} key={id} className="input__item" style={{ display: 'block' }} {...props}>
            <Input
                onClick={(): void => {
                    handleToggleShowDropDown();
                }}
                id={id}
                styleinput={StyleInput.TEXT}
                readOnly
                name={name}
                value={value}
                style={style}
            ></Input>
            <Arrow
                style={{
                    position: 'absolute',
                    right: '15px',
                    top: '50%',
                    height: '7px',
                    width: '7px',
                    transform: 'rotate(-45deg) translate(70%, -10%)',
                    borderColor: '#462929',
                }}
            ></Arrow>
            {props.children}
        </label>
    );
};

export default InputDropDownListWrapper;
