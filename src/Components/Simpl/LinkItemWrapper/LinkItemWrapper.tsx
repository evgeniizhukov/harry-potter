import { FC, LiHTMLAttributes } from 'react';
import { Link } from 'react-router-dom';

interface ILinktem extends LiHTMLAttributes<HTMLLIElement> {
    url: string;
}

const Linktem: FC<ILinktem> = ({ url, ...props }) => {
    return (
        <li className={'header__item'} {...props}>
            <Link style={{ textDecoration: 'none', color: '#B09A81', fontSize: '20px' }} to={url}>
                {props.children}
            </Link>
        </li>
    );
};

export default Linktem;
