import React, { FC, HTMLAttributes } from 'react';

interface INavBarWrapper extends HTMLAttributes<HTMLDivElement> {}

const NavBarWrapper: FC<INavBarWrapper> = ({ ...props }): React.ReactElement => {
    return (
        <nav className={'header__nav'} {...props}>
            {props.children}
        </nav>
    );
};

export default NavBarWrapper;
