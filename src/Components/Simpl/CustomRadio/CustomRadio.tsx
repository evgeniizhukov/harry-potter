import React, { AllHTMLAttributes, FC, useState } from 'react';

import Input from '../../UI/Input/Input';

interface ICustomRadio extends AllHTMLAttributes<HTMLLabelElement> {
    name: string;
    id: string;
    value: string;
    setShowDropDownList?: () => void;
    setValue: (value: string) => void;
}

const CustomRadio: FC<ICustomRadio> = ({ name, id, value, setShowDropDownList, setValue, ...props }) => {
    const [checked, setChecked] = useState(false);

    const changeCheckbox = (e: HTMLInputElement): void => {
        if (e.matches('input')) {
            setChecked(!checked);
            setValue(value);
            setShowDropDownList ? setShowDropDownList() : null;
        }
    };
    return (
        <label
            onClick={(e: React.MouseEvent): void => {
                changeCheckbox(e.target as HTMLInputElement);
            }}
            className="option"
            htmlFor={id}
            {...props}
        >
            <Input readOnly id={id} defaultChecked={checked} type={'radio'} name={name} value=""></Input>

            <span className="option__checkbox"></span>
            {props.children}
        </label>
    );
};

export default CustomRadio;
