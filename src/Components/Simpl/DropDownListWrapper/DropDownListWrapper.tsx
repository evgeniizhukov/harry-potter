import { AllHTMLAttributes, FC } from 'react';

interface IDropDownListWrapper extends AllHTMLAttributes<HTMLDivElement> {
    amountElements: number;
    heightElement: number;
    showDropDownList: boolean;
}

const heightDropDown = (show: boolean, heightElement: number, amountElements: number): string => {
    return show ? `${heightElement * amountElements}px` : '0px';
};
const DropDownListWrapper: FC<IDropDownListWrapper> = ({
    amountElements,
    heightElement,
    showDropDownList,
    ...props
}) => {
    return (
        <div
            className={'select'}
            {...props}
            style={
                showDropDownList
                    ? {
                          boxShadow: '0 0 8px 2px',
                          height: heightDropDown(showDropDownList, heightElement, amountElements),
                      }
                    : { height: heightDropDown(showDropDownList, heightElement, amountElements) }
            }
        >
            {props.children}
        </div>
    );
};

export default DropDownListWrapper;
