import React, { FC, ImgHTMLAttributes } from 'react';
import { Link } from 'react-router-dom';

interface ILogo extends ImgHTMLAttributes<HTMLImageElement> {
    url: string;
}

const Logo: FC<ILogo> = ({ url, ...props }) => {
    return (
        <Link to={url} className="header__logo">
            <img {...props} className="header__img" />
        </Link>
    );
};

export default Logo;
