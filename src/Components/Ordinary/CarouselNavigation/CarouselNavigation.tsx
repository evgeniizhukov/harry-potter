import { AllHTMLAttributes, FC } from 'react';

import Arrow from '../../UI/Arrow/Arrow';
import Pagination from '../Pagination/Pagination';

import { changeNextSlide, changePrevSlide } from './carouselNavigation.function';

interface ICarouselNavigation extends AllHTMLAttributes<HTMLDivElement> {
    lengthSlider: number;
    currentSlide: number;
    showCardOnOneSlide: number;
    setNumberSlide: (numberSlide: number) => void;
    numberDotMaximum: number;
}

const CarouselNavigation: FC<ICarouselNavigation> = ({
    lengthSlider,
    currentSlide,
    showCardOnOneSlide,
    setNumberSlide,
    numberDotMaximum,
    ...props
}) => {
    return (
        <div className="slider__nav" {...props}>
            <Arrow
                onClick={(): void => {
                    changePrevSlide(currentSlide, showCardOnOneSlide, setNumberSlide);
                }}
                style={{ height: '17px', width: '17px' }}
            ></Arrow>
            <Pagination
                numberDotMaximum={numberDotMaximum}
                currentSlide={currentSlide}
                lengthSlider={lengthSlider}
                setNumberSlide={setNumberSlide}
                showCardOnOneSlide={showCardOnOneSlide}
            ></Pagination>

            <Arrow
                onClick={(): void => {
                    changeNextSlide(currentSlide, lengthSlider, showCardOnOneSlide, setNumberSlide);
                }}
                style={{ transform: 'rotate(-135deg)', height: '17px', width: '17px' }}
            ></Arrow>
        </div>
    );
};

export default CarouselNavigation;
