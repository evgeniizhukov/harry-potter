export const changeNextSlide = (
    currentSlide: number,
    lengthSlider: number,
    showCardOnOneSlide: number,
    setNumberSlide: (numberSlide: number) => void
): void => {
    if (currentSlide < lengthSlider - showCardOnOneSlide) {
        setNumberSlide(currentSlide + showCardOnOneSlide);
    }
};
export const changePrevSlide = (
    currentSlide: number,
    showCardOnOneSlide: number,
    setNumberSlide: (numberSlide: number) => void
): void => {
    if (currentSlide >= showCardOnOneSlide) {
        setNumberSlide(currentSlide - showCardOnOneSlide);
    }
};
