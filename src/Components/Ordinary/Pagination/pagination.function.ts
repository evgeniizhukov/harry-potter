import { INCREMENT } from '../../../Core/Constants/constants';

export const handleMoveSlider = (
    i: number,
    currentCircleMaximum: number,
    numberSlide: number,
    setNumberSlide: (numberSlide: number) => void,
    showCardOnOneSlide: number,
    lengthSlider: number
): void => {
    const center: number = (currentCircleMaximum - INCREMENT) / 2;
    // если мы в середине и двигаемся правее
    if (numberSlide / showCardOnOneSlide >= center && lengthSlider > showCardOnOneSlide + numberSlide) {
        setNumberSlide(numberSlide + (i - center) * showCardOnOneSlide);
    }
    // если мы вначале слайда и до середины
    if (lengthSlider >= i * showCardOnOneSlide && !numberSlide && i) {
        setNumberSlide(numberSlide + i * showCardOnOneSlide);
    }
    // если мы в конце слайдера и до середины

    if (
        numberSlide - (currentCircleMaximum - INCREMENT - i) * showCardOnOneSlide >= 0 &&
        lengthSlider - numberSlide <= showCardOnOneSlide &&
        i !== currentCircleMaximum - INCREMENT
    ) {
        setNumberSlide(numberSlide - (currentCircleMaximum - i - INCREMENT) * showCardOnOneSlide);
    }
};

export const toSetBackgroungCircle = (
    i: number,
    numberSlide: number,
    showCardOnOneSlide: number,
    lengthSlider: number,
    currentCircleMaximum: number
): string => {
    if (numberSlide === 0 && i === 0) {
        return '';
    }
    if (lengthSlider - numberSlide <= showCardOnOneSlide && i === currentCircleMaximum - 1) {
        return '';
    }
    if (lengthSlider - numberSlide > showCardOnOneSlide && numberSlide !== 0 && i === (currentCircleMaximum - 1) / 2) {
        return '';
    }

    return 'white';
};
