import { FC, HTMLAttributes, ReactElement } from 'react';

import PaginationItem from '../../UI/PaginationItem/PaginationItem';

import { handleMoveSlider, toSetBackgroungCircle } from './pagination.function';

interface IPagination extends HTMLAttributes<HTMLDivElement> {
    numberDotMaximum: number;
    currentSlide: number;
    showCardOnOneSlide: number;
    lengthSlider: number;
    setNumberSlide: (numberSlide: number) => void;
}

const Pagination: FC<IPagination> = ({
    numberDotMaximum,
    currentSlide,
    showCardOnOneSlide,
    lengthSlider,
    setNumberSlide,
    ...props
}): ReactElement => {
    const newArray: string[] = [];

    for (let i = 0; i < numberDotMaximum && i < lengthSlider / showCardOnOneSlide; i++) {
        newArray.push('');
    }

    return (
        <div className={'slider__items'} style={{ width: '100%' }} {...props}>
            {newArray.map((_, i) => (
                <PaginationItem
                    key={i}
                    style={{
                        background: toSetBackgroungCircle(
                            i,
                            currentSlide,
                            showCardOnOneSlide,
                            lengthSlider,
                            newArray.length
                        ),
                    }}
                    onClick={(): void => {
                        handleMoveSlider(
                            i,
                            newArray.length,
                            currentSlide,
                            setNumberSlide,
                            showCardOnOneSlide,
                            lengthSlider
                        );
                    }}
                ></PaginationItem>
            ))}
        </div>
    );
};

export default Pagination;
