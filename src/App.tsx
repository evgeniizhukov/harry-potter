import './style/sass/main.sass';
import { Provider } from 'react-redux';

import { store } from './Core/redux/store';
import Routes from './Routes/Routes';

const App = (): React.ReactElement => {
    return (
        <Provider store={store}>
            <Routes />
        </Provider>
    );
};

export default App;
